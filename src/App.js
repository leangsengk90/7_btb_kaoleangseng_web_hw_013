import './App.css';
import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Main from './components/Main';
import 'bootstrap/dist/css/bootstrap.min.css';
import News from './components/News';
import AddNews from './components/AddNews';
import View from './components/View';
import Search from './components/Search';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <Main />
          <Switch>
            <Route path="/" exact component={News} />
            <Route path="/search" component={Search} />
            <Route path="/addnews" component={AddNews}/>
            <Route path="/view/:id" component={View}/>
          </Switch>
        </Router>

      </div>
    )
  }
}

export default App;
