import React, { Component } from "react";
import { Navbar, Form, Button, Nav, FormControl } from "react-bootstrap";
import {Link} from 'react-router-dom';

export default class Main extends Component {
  constructor(){
    super();
    this.state = {
      search: "",
    }
  }

  onChangeText(e){
    this.setState({
      [e.target.name]: e.target.value,
    })
  }

  render() {
    return (
      <div style={{marginBottom: '25px'}}>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand as={Link} to="/">News</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
          </Nav>
          <Form inline>
            <FormControl type="text" name="search" onChange={this.onChangeText.bind(this)} placeholder="Search by ID" className="mr-sm-2" />
            <Link to={`/search/?search=${this.state.search}`}><Button variant="outline-info">Search</Button></Link>
          </Form>
        </Navbar>
      </div>
    );
  }
}
