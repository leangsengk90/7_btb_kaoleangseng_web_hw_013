import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import Axios from "axios";
import { Redirect } from "react-router-dom";
import queryString from "query-string";

export default class AddNews extends Component {
  constructor() {
    super();
    this.state = {
      id:"",
      title: "",
      desc: "",
      url: "",
      noImage: "https://www.afmec.org/images/no-image-available-icon.jpg",
      sms: "",
      comfirmBox: false,
      isUpdate: false,
    };
  }

  componentDidMount() {
    let query = queryString.parse(this.props.location.search);
    if (query.update === "true") {
      let myId = `${query.id}`;
      Axios.get(`http://110.74.194.124:15011/v1/api/articles/${myId}`).then(
        (result) => {
          //console.log(result.data.DATA);
            this.setState({
              isUpdate: true,
              title: result.data.DATA.TITLE,
              desc: result.data.DATA.DESCRIPTION,
              url: result.data.DATA.IMAGE,
              id: result.data.DATA.ID,
            });
        }
      ).catch(error=>{
        console.log(error);
      });
    }
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  onChangeImage(e) {
    this.setState({
      [e.target.name]: e.target.value,
      url: e.target.value,
    });
  }

  addRecord = () => {
    if (this.state.title !== "" && this.state.desc !== "") {
      if (this.state.url === "") {
        this.state.url = this.state.noImage;
      }
      let record = {
        TITLE: this.state.title,
        DESCRIPTION: this.state.desc,
        IMAGE: this.state.url,
      };
      if (this.state.isUpdate) {
        Axios.put(`http://110.74.194.124:15011/v1/api/articles/${this.state.id}`, record).then(
        (result) => {
            if (
              window.confirm(
                `${result.data.DATA.ID}: ${result.data.MESSAGE}\n😘 Do you want to go Home Page? 😘`
              )
            ) {
              this.setState({ comfirmBox: true });
            } else {
              this.setState({ comfirmBox: false });
            }
          this.setState({
            title: "",
            desc: "",
            url: "",
          });
        }
      ).catch(error=>{console.log(error)});
      } else {
        Axios.post("http://110.74.194.124:15011/v1/api/articles", record).then(
          (result) => {
              if (
                window.confirm(
                  `${result.data.DATA.ID}: ${result.data.MESSAGE}\n😘 Do you want to go Home Page? 😘`
                )
              ) {
                this.setState({ comfirmBox: true });
              } else {
                this.setState({ comfirmBox: false });
              }
            this.setState({
              title: "",
              desc: "",
              url: "",
            });
          }
        ).catch(error=>{
          console.log(error);
        });
      }
    } else {
      this.setState(() => {
        return (this.state.sms = "Please input something!");
      });
    }
  };

  setImageError(e) {
    e.target.src = this.state.noImage;
  }

  render() {
    if (this.state.comfirmBox) {
      return <Redirect to="/" />;
    }
    return (
      <div className="container">
        <div className="row text-left">
          <div className="col-md-8">
            <h1>{this.state.isUpdate ? "Update" : "Add"} News</h1>
            <Form>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>TITLE</Form.Label>
                <Form.Control
                  type="text"
                  name="title"
                  value={`${this.state.title}`}
                  onChange={this.onChange.bind(this)}
                  placeholder="Enter title"
                />
                <Form.Text className="text-sms">
                  {this.state.title === "" ? this.state.sms : ""}
                </Form.Text>
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>DESCRIPTION</Form.Label>
                <Form.Control
                  type="text"
                  name="desc"
                  value={`${this.state.desc}`}
                  onChange={this.onChange.bind(this)}
                  placeholder="Enter description"
                />
                <Form.Text className="text-sms">
                  {this.state.desc === "" ? this.state.sms : ""}
                </Form.Text>
              </Form.Group>
              <Button
                onClick={this.addRecord.bind(this)}
                variant="primary"
                type="button"
                style={{ width: "200px" }}
              >
                {this.state.isUpdate ? "Update" : "Add"}
              </Button>
            </Form>
          </div>
          <div className="col-md-4" style={{ marginTop: "25px" }}>
            <img
              style={{ marginBottom: "15px" }}
              onError={this.setImageError.bind(this)}
              src={
                this.state.url === null ? this.state.noImage : this.state.url
              }
              width="300px"
              height="200px"
            />
            <textarea
              style={{ width: "300px" }}
              name="url"
              value={`${this.state.url}`}
              onChange={this.onChangeImage.bind(this)}
              placeholder="Image URL"
            />
          </div>
        </div>
      </div>
    );
  }
}
