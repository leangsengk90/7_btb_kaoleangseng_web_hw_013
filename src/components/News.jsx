import React, { Component } from "react";
import TableView from "./TableView";
import Axios from "axios";
import { Button } from "react-bootstrap";
import Moment from "react-moment";
import { Link } from "react-router-dom";

export default class News extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      image: "",
      noImage: "https://www.afmec.org/images/no-image-available-icon.jpg",
    };
  }

  componentDidMount() {
    Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
      .then((result) => {
        //console.log(result.data.DATA);
        this.setState({
          data: result.data.DATA,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  onDelete(index, e) {
    if (window.confirm("😱 Do you want to delete this News? 😱")) {
      //console.log("INDEX: "+index);
      this.setState(this.state.data.splice(index, 1));
      let getId = e.target.name;
      Axios.delete(`http://110.74.194.124:15011/v1/api/articles/${getId}`)
        .then((result) => {
          console.log(result.data.DATA);
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
    }
  }

  setImageError(e) {
    e.target.src = this.state.noImage;
  }

  render() {
    let myData = this.state.data.map((d, index) => (
      <tr key={d.ID}>
        <td>{`${d.ID}`}</td>
        <td>{`${d.TITLE}`}</td>
        <td>{`${d.DESCRIPTION}`}</td>
        <td>
          <Moment format="YYYY-MM-DD">
            {`${d.CREATED_DATE}`.substring(0, 7)}
          </Moment>
        </td>
        <td>
          <img
            onError={this.setImageError.bind(this)}
            src={d.IMAGE === null ? this.state.noImage : d.IMAGE}
            width="200px"
            height="150px"
          />
        </td>
        <td>
          <div className="row">
            <div className="col">
              <Link to={{ pathname: `/view/${d.ID}` }}>
                <Button variant="primary" name={d.ID}>
                  View
                </Button>{" "}
              </Link>
              <Link to={`/addnews?update=true&&id=${d.ID}`}>
                <Button variant="warning" name={d.ID}>
                  Edit
                </Button>{" "}
              </Link>
              <Button
                variant="danger"
                name={d.ID}
                onClick={this.onDelete.bind(this, index)}
              >
                Delete
              </Button>
            </div>
          </div>
        </td>
      </tr>
    ));
    return <TableView dataRows={myData} />;
  }
}
