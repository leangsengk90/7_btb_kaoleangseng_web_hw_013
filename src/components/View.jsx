import React, { Component } from "react";
import Moment from "react-moment";
import Axios from "axios";

export default class View extends Component {
  constructor() {
    super();
    this.state = {
      noImage: "https://www.afmec.org/images/no-image-available-icon.jpg",
      id: "",
      title: "",
      desc: "",
      date: "",
      image: "",
    };
  }

  componentDidMount() {
    let myId = this.props.match.params.id;
    Axios.get(`http://110.74.194.124:15011/v1/api/articles/${myId}`).then(
      (result) => {
        //console.log(result.data);
          this.setState({
            id: result.data.DATA.ID,
            title: result.data.DATA.TITLE,
            desc: result.data.DATA.DESCRIPTION,
            date: result.data.DATA.CREATED_DATE,
            image: result.data.DATA.IMAGE,
          });
      }
    ).catch(error=>{console.log(error)});
  }

  setImageError(e) {
    e.target.src = this.state.noImage;
  }

  render() {
    return (
      <div className="container">
        <div className="row" style={{ textAlign: "left" }}>
          <div className="col-md-4">
            <img
              onError={this.setImageError.bind(this)}
              src={
                this.state.image === null
                  ? this.state.noImage
                  : this.state.image
              }
              width="300px"
              height="200px"
            />
          </div>
          <div className="col-md-8">
            <h3>{this.state.title}</h3>
            <p>
              #{this.state.id} <br /> Date:{" "}
              {
                <Moment format="YYYY-MM-DD">
                  {`${this.state.date}`.substring(0, 7)}
                </Moment>
              }
            </p>
            <h5>{this.state.desc}</h5>
          </div>
        </div>
      </div>
    );
  }
}
