import React, { Component } from "react";
import { Table, Button, Pagination } from "react-bootstrap";
import { Link } from "react-router-dom";

export default class TableView extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-4"></div>
          <div className="col-md-4">
            <h1>Hot News</h1>
          </div>
          <div className="col-md-4 text-right align-self-center">
            <Link to="/addnews">
              <Button
                variant="success"
                style={{ width: "200px", marginRight: "25px" }}
              >
                Add News
              </Button>
            </Link>
          </div>
        </div>
        <div className="row">
          <div className="col-md">
            <Table striped bordered hover variant="dark">
              <thead>
                <tr>
                  <th style={{ width: "5%" }}>#</th>
                  <th style={{ width: "15%" }}>TITLE</th>
                  <th style={{ width: "25%" }}>DESCRIPTION</th>
                  <th style={{ width: "15%" }}>CREATED DATE</th>
                  <th style={{ width: "15%" }}>IMAGE</th>
                  <th style={{ width: "25%" }}>ACTION</th>
                </tr>
              </thead>
              <tbody>{this.props.dataRows}</tbody>
            </Table>
          </div>
        </div>
        <div className="row">
          <div className="col-md-4" style={{margin: '0 auto'}}>
            <Pagination>
              <Pagination.First />
              <Pagination.Prev />
              <Pagination.Item active>{1}</Pagination.Item>
              <Pagination.Item>{2}</Pagination.Item>
              <Pagination.Item>{3}</Pagination.Item>
              <Pagination.Item>{4}</Pagination.Item>
              <Pagination.Item>{5}</Pagination.Item>
              <Pagination.Next />
              <Pagination.Last />
            </Pagination>
          </div>
        </div>
      </div>
    );
  }
}
